#include<stdio.h>
#include<netinet/in.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>


#define PORT 44444
#define IP	 "127.0.0.1"
#define PACKETMAX	512
#define PACKETMIN	4

typedef struct buffer{
	char buf[1024] ;
	int startPos ;
	int endPos ;
	int max ;
}Buffer_s;

typedef struct Conn
{
	int fd ;
	Buffer_s *buffer ;
	int lConnState ;
	int revents ;
}CONN;

int createServSock()
{
	
	int servSock = -1 ;
	servSock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if( servSock < -1)
	{
		printf("create sock is error:%s",strerror(errno)) ;
		exit(-1) ;
	}
	
	return servSock ;
}

int tcpRecv(CONN *conn )
{
	if (NULL == conn)
	{
		printf("error conn is NULL\n") ;
		return -1 ;
	}

	int nread = recv(conn->fd,conn->buffer->buf+conn->buffer->endPos,1024-conn->buffer->endPos,0) ;
	if (nread <0)
	{
		int status = errno ;
		if ((status==EINTR) || (status==EWOULDBLOCK) ||(status == EAGAIN))
		{
			printf("%d socket is error:%s\n",conn->fd,strerror(status)) ;
			return 0 ;
		} 

		/* other errors*/
		close(conn->fd) ;
		conn->fd = -1 ;
		return -1 ;
	}
	else if(nread ==0)
	{
		printf("%d socket is eof\n",conn->fd) ;
		close(conn->fd) ;
		conn->fd = -1 ;
		return -1 ;
		
	}
	else
	{
		conn->buffer->endPos += nread ;
		conn->buffer->buf[conn->buffer->endPos] = '\0' ;
	}
	return 0 ;
}


int dealMessage(CONN *pConn ,int liv_MessageLen )
{
	/* deal message
		*	
		*
		deal end
	*/

	printf("dealMessage,headLen:%d,bodyLen:%d bodyMessage:%s\n",htons(*(unsigned short*)(pConn->buffer->buf)),htons(*(unsigned short*)(pConn->buffer->buf+2)),pConn->buffer->buf+4) ;

	/* update the buffer pointer*/
	pConn->buffer->startPos += liv_MessageLen + PACKETMIN ;
	printf("startPos:%p,endPos:%p\n",pConn->buffer->buf+pConn->buffer->startPos,pConn->buffer->buf+pConn->buffer->endPos) ;
	return 0 ;
}

int checkBuf(Buffer_s *buffer)
{
	if (NULL == buffer)
	{
		printf("The buffer is error\n") ;
		return 0 ;
	}

	if(buffer->startPos == buffer->endPos)
	{
		buffer->startPos = buffer->endPos = 0 ;
		return 0 ;
	}

	if ( buffer->startPos != 0 )
	{
		memmove(buffer->buf,buffer->buf + buffer->startPos, buffer->endPos  - buffer->startPos) ;
		buffer->endPos = buffer->endPos - buffer->startPos ;
		buffer->startPos = 0 ;
	}

	if ((buffer->endPos - buffer->startPos) > 0 )
	{
		return 1 ;
	}

	return 0 ;
}

int recvMessage(CONN *pConn)
{
		/* recv data from conn*/
		if ( -1 == tcpRecv(pConn))
		{
			return -1 ;
		}
		int liv_DataLen = 0 ;
DealMessageAgain:
		printf("error\n") ;
		liv_DataLen = pConn->buffer->endPos - pConn->buffer->startPos ;
		if (liv_DataLen < PACKETMIN)
		{
			printf("message header not recieve completed,continue to recv\n") ;
			return -1 ;
		}

		int liv_HeadLen = ntohs(*(unsigned short*)(pConn->buffer->buf + pConn->buffer->startPos)) ;
		int liv_BodyLen = ntohs(*(unsigned short*)(pConn->buffer->buf + pConn->buffer->startPos +2)) ;
			
		int liv_MessageLen = liv_HeadLen + liv_BodyLen ;
		if ( (liv_MessageLen < 0) || (liv_MessageLen > PACKETMAX)) /* maybe the data is dirty*/
		{
			printf("maybe the data is dirty,socket:%d\n",pConn->fd) ;
			close(pConn->fd) ;
			pConn->fd = -1 ;
			return -1 ;
		}

		if (liv_DataLen < liv_MessageLen) /* may be the package is not recv compeleted*/
		{
			printf("the message is not compelete, %d need to recv!\n",liv_MessageLen - liv_DataLen) ;
			return -1 ;
		}

		int ret = dealMessage(pConn,liv_MessageLen) ; /*if parse data is error, we will close the conn.*/
		if (ret < 0)
		{
			close(pConn->fd) ;
			pConn->fd = -1 ;
			return -1 ;
		}
		printf("%d startPos:%p,endPos:%p\n",__LINE__,pConn->buffer->buf+pConn->buffer->startPos,pConn->buffer->buf+pConn->buffer->endPos) ;
		if (checkBuf(pConn->buffer))
		{
			goto DealMessageAgain ;
		}
		
		return 0 ;
}	

int sendMessage(int fd,Buffer_s *buf) //Writen
{
	int nWrited = 0 ;
	fd_set writeFd ;
	int left = buf->endPos - buf->startPos;
	char *ptr = buf->buf+buf->startPos ;
	while(left >0)
	{
		nWrited = send(fd,ptr,left,0) ;
		if(nWrited <0)
		{
			if(errno == EINTR || errno == EAGAIN  || errno == EWOULDBLOCK)
			{
				printf("recv the error:%s",strerror(errno)) ;
				FD_ZERO(&writeFd) ;
				FD_SET(fd,&writeFd) ;
				int ret = select(fd+1,NULL,&writeFd,NULL,NULL) ;
				if (ret < 0 )	/* some is error */
				{
					printf("tcp connect is error\n") ;
					close(fd) ;
					fd = -1 ;
					return -1 ;
				}
				else if (ret ==0 )  /* time out */
				{
					printf("select is time out\n") ;
					return -1 ;
				}
				else /* fd can write again */
				{
					continue ;
				} // end if ret
			}
			else
			{
				printf("recv the error:%s",strerror(errno)) ;
				close(fd) ;
				exit(-1) ;
			}
		}
		else if(nWrited == 0 )
		{
			close(fd) ;
			printf("the socket is closed!") ;
			return 0 ;
		}
		else
		{
			printf("Write %d byte data\n",nWrited) ;
			left -= nWrited ;
			ptr += nWrited ;
		}
	}
	buf->startPos = buf->endPos = 0 ;
	printf("data is writed\n");
	return 0 ;
}


int main()
{
	int ret = -1 ;
	CONN conn ;
	conn.buffer = (Buffer_s *)malloc(sizeof(Buffer_s)) ;
	memcpy(conn.buffer->buf, "luojian is a smaller",strlen("luojian is a smaller")) ;
	conn.buffer->endPos= strlen(conn.buffer->buf) ;
	conn.buffer->max = 1024 ;
	conn.buffer->startPos = 0 ;
	
	int servSock = -1 ;
	struct sockaddr_in servAddr ;
	struct sockaddr_in localAddr;
	
	memset(&servAddr,0,sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(PORT) ;
	servAddr.sin_addr.s_addr = inet_addr(IP) ;

	conn.fd = createServSock() ;
again:	
	ret = connect(conn.fd,(struct sockaddr *)&servAddr,sizeof(servAddr)) ;
	
	if (ret < 0 )
	{
		if(errno == EINTR || errno == EAGAIN)
		{
			goto again;
		}
		else
		{
			printf("some error:%s",strerror(errno)) ;
			exit(-1) ;
		}
	}

	memset(&localAddr,0,sizeof(localAddr)) ;
	socklen_t len = sizeof(localAddr) ;
	ret = getsockname(conn.fd,(struct sockaddr*)&localAddr,&len) ;
	if(ret <0)
	{
		printf("connect to serv is ok") ;
	}
	else
	{
		printf("connect to serv is ok,and local addr is:%s",inet_ntoa(localAddr.sin_addr)) ;
	}

	sendMessage(conn.fd,conn.buffer);
	recvMessage(&conn) ;

	close(conn.fd) ;
	return 0 ;
}