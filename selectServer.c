#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<string.h>
#include<sys/time.h>
#include<sys/select.h>
#include<unistd.h>
#include<errno.h>

#define SIZE 10 

typedef struct serv_context{
	int cli_cnt ;			//客户端个数
	int clifds[SIZE] ;		//客户端
	fd_set allfds ;
	int maxfd ;				//句柄最大数
}serv_context_st ;

static serv_context_st *s_src_ctx =NULL;

/*
	functionName : createsock
	description:   create socket and change the socket attribute; bind the address
	Input		: port, ip
	Output		: success : fd, error : -1
*/
int createSock(int port,char *ip)
{
	int sock =-1;
	int buffsize = 1024 ;
	int optive = 1 ;
	struct sockaddr_in servAddr ;
	sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if(sock<0)
	{
		printf("[%s][%d]error create socket",__FILE__,__LINE__) ;
		return -1 ;
	}

	int ret = setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,(void*)&optive,sizeof(optive)) ;
	if(ret <0)
	{
		printf("[%s][%d]setsocket opt SO_REUSERADDR is error\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	ret = setsockopt(sock,SOL_SOCKET,SO_RCVBUF,&buffsize,sizeof(buffsize)) ;
	if(ret<0)
	{
		printf("[%s][%d]setsockopt is error SO_RECVBUF\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	ret = setsockopt(sock,SOL_SOCKET,SO_SNDBUF,&buffsize,sizeof(buffsize)) ;
	if(ret<0)
	{
		printf("[%s][%d] setsockopt is error SO_SNDBUF\n",__FILE__,__LINE__);
		return -1 ;
	}

	bzero(&servAddr,sizeof(servAddr)) ;
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port) ;		//网络字节序
	servAddr.sin_addr.s_addr= inet_addr(ip) ;
	ret = bind(sock,(struct sockaddr *)&servAddr,sizeof(servAddr)) ;
	if(ret<0)
	{
		printf("[%s][%d]bind is error",__FILE__,__LINE__) ;
		close(sock) ;
		return -1 ;
	}
	
	return sock ;
}


int uninit()
{
	if(s_src_ctx)
	{
		free(s_src_ctx) ;
		s_src_ctx = NULL;
	}
	return 0 ;
}

int init()
{
	int i = 0 ;

	s_src_ctx = (serv_context_st*)malloc(sizeof(serv_context_st));
	if(NULL == s_src_ctx){
		fprintf(stderr,"malloc the context is error") ;
		return -1 ;
	}
	
	memset(s_src_ctx,0,sizeof(serv_context_st)) ;
	
	for(i=0;i<SIZE;i++)
	{
		s_src_ctx->clifds[i]= -1 ;
	}
	
	return 0 ;
}

int accept_client_proc(int servfd)
{
	int clifd = -1 ;
	struct sockaddr_in cliAddr ;
	socklen_t cliAddrLen ;
	cliAddrLen = sizeof(cliAddr) ;
	memset(&cliAddr,0,sizeof(struct sockaddr_in));

	printf("accept client proc is called!\n") ;
ACCEPT:
	clifd = accept(servfd,(struct sockaddr*)&cliAddr,&cliAddrLen) ;

	if(-1 == clifd)
	{
		if(EINTR==errno)
		{
			goto ACCEPT ;
		}
		else
		{
			fprintf(stderr,"accept fail error:%s",strerror(errno));
			return -1 ;
		}
	}

	fprintf(stdout,"accept a new client:%s:%d\n",inet_ntoa(cliAddr.sin_addr),ntohs(cliAddr.sin_port)) ;
	int i = 0;
	for (i = 0 ;i<SIZE;i++)
	{
		if(s_src_ctx->clifds[i] == -1)
		{
			s_src_ctx->clifds[i] = clifd ;
			s_src_ctx->cli_cnt++ ;
			break ;
		}
	}
	if (i == SIZE)
	{
		fprintf(stderr,"too many clients.\n") ;
		return -1 ;
	}
	return 0 ;
}

int handleCliMsg(int clifd,char *recvBuf,int dataLen)
{
	struct sockaddr_in cliAddr ;
	socklen_t cliAddrLen ;
	char sendBuf[4096] ;
	getpeername(clifd,(struct sockaddr*)&cliAddr,&cliAddrLen) ;
	fprintf(stdout,"from %s:%d recv buf :%s\n",inet_ntoa(cliAddr.sin_addr),
		ntohs(cliAddr.sin_port),recvBuf) ;
	
	snprintf(sendBuf+4,4096,"i have recv:%s",recvBuf);
	*(unsigned short*)sendBuf = htons(0) ;
	*(unsigned short*)(sendBuf+2) = htons(strlen(sendBuf+4)) ;
	int left = strlen(sendBuf+4) + 4; 
	int pos = 0 ;
	while(left >0)
	{
		int writeDataLen = write(clifd,sendBuf+pos,left) ;
		if (writeDataLen <0)
		{
			if((errno==EINTR) || (errno == EWOULDBLOCK) || (errno==EAGAIN))
			{
				fd_set writeFds ;
				FD_ZERO(&writeFds) ;
				FD_SET(clifd,&writeFds) ;
				
				struct timeval  tv ;
				tv.tv_sec = 10 ;
				tv.tv_usec = 0 ;
				int ret  = select(clifd+1,NULL,&writeFds,NULL,&tv) ;
				if (ret > 0 )		//success
				{
					continue ;
				}
				else if(ret == 0)  //time out
				{
					close(clifd) ;
					return -1 ;
				}
				else				// error
				{
					close(clifd) ;
					return -1 ;
				}
			}
			else
			{
				close(clifd) ;
				return -1 ;
			}
		} 
		else if (writeDataLen == 0)
		{
			close(clifd) ;
			return -1 ;
		}
		else 
		{
			left -= writeDataLen ;
			pos += writeDataLen ;
		}
	}
	
	return 0 ;	
}

int recv_client_msg(fd_set* readFd)
{
	int i = 0  ;
	int clifd = -1 ;
	int dataLen = 0 ;
	char recvBuf[4096] ; //recv buffer
	char *ptr = recvBuf ;
	int leftLen = 4096 ;
	memset(recvBuf,0,4096);
	for(i = 0 ;i<SIZE ;i++)
	{
		clifd = s_src_ctx->clifds[i] ;
		if(clifd <0)
		{
			continue ;
		}

		if(FD_ISSET(clifd,readFd))
		{
				dataLen = read(clifd,ptr,leftLen) ;
				if(dataLen<0)  // some error
				{
					if(errno == EINTR || errno == EWOULDBLOCK || errno == EAGAIN)
					{
						continue ;
					}
					else
					{
						FD_CLR(clifd,readFd) ;
						close(clifd) ;
						s_src_ctx->clifds[i] = -1 ;
						return 1 ;
					}
				}
				else if(dataLen==0)// end of EOF  // the client closed
				{
					printf("the client sock is close\n") ;
					close(clifd) ;
					s_src_ctx->clifds[i]=-1 ;
					break ;
				}
				printf("recv the data len is :%d\n",dataLen) ;
			
				handleCliMsg(clifd,recvBuf,dataLen) ; //you can deal it asny
				return 0 ; // do it success
		}
	}
	if(SIZE==i)
	{
		fprintf(stderr,"some wrong with the session") ;
		return 1 ;
	}
	return 1 ;
}


int closeAll()
{
	int i = 0 ;
	int clifd = -1 ;
	for (i = 0 ;i<SIZE ;i++)
	{
		clifd = s_src_ctx->clifds[i] ;
		close(clifd) ;
		s_src_ctx->clifds[i] = -1 ;
	}
	return 0 ;
}


int handle_client_proc(int servfd)
{
	fd_set *readFd = &s_src_ctx->allfds;
	int clifd = -1 ;
	int i = 0 ;
	int retval = -1 ;
	//set the time out
	struct timeval timeout ;
	
	while(1){
		FD_ZERO(readFd) ;
		FD_SET(servfd,readFd) ;//add the listen fd
		s_src_ctx->maxfd = servfd ;

		for(i = 0 ;i<s_src_ctx->cli_cnt ;i++)
		{
			clifd = s_src_ctx->clifds[i] ;
			if (clifd != -1)
			{
				FD_SET(clifd,readFd) ;
			}
			// find the max fd
			s_src_ctx->maxfd = (clifd > s_src_ctx->maxfd? clifd:s_src_ctx->maxfd) ;
		}
		timeout.tv_sec = 10;//秒
		timeout.tv_usec = 0;//微秒
		
		retval = select(s_src_ctx->maxfd+1,readFd,NULL,NULL,&timeout) ;
		if(retval<0)
		{
			if(errno==EAGAIN || errno== EINTR|| errno==EWOULDBLOCK)
			{
				continue ;
			}
			else // some else error
			{
				fprintf(stderr,"select is error:%d\n",strerror(errno)) ;
				close(servfd) ;
				closeAll() ;
				return ;
			}
		}
		else if(retval==0) // time out
		{
			fprintf(stdout,"select is timeout:%d\n",strerror(errno)) ;
			continue ;
		}
		else
		{
			printf("some one is recv\n") ;
			switch(FD_ISSET(servfd,readFd))
			{
				case 0: //handle the server message 
					recv_client_msg(readFd) ;
					break ;
				default://handle the client massage
					accept_client_proc(servfd);				
			}
		}
	}
	return 0 ;
}


int main()
{
	int port = 44444;
	char *ip = "127.0.0.1";
	int listenFd = -1 ;

	init(s_src_ctx) ;

	listenFd = createSock(port,ip) ;
	listen(listenFd,5) ;

	handle_client_proc(listenFd) ;
	
	close(listenFd);
	
	uninit() ;
	return 0 ;
}